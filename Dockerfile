FROM python:3.7-slim-bullseye
LABEL MAINTAINER "CRAPP.cloud" EMAIL:digiaplicacion@gmail.com <Jose Rojas>

SHELL ["/bin/bash", "-xo", "pipefail", "-c"]

# Generate locale C.UTF-8 for postgres and general locale data
ENV LANG C.UTF-8
ENV TZ=America/Costa_Rica
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get -qq install keyboard-configuration
# Install some deps, lessc and less-plugin-clean-css, and wkhtmltopdf
RUN set -x; \
        apt-get update \
        && apt-get install -y --no-install-recommends apt-utils \
            ca-certificates \
            curl \
            dirmngr \
            fonts-noto-cjk \
            gcc \
            git \
            gnupg \
            dialog \
            libevent-dev \
            libjpeg-dev \
            libldap2-dev \
            libpng-dev \
            libpq-dev \
            libsasl2-dev \
            libssl-dev \
            libxml2-dev \
            libxrender1 \
            libxslt1-dev \
            libxext6 \
            fontconfig \
            node-less \
            npm \
            postgis \
            python3-dev \
            python3-pip \
            python3-pyldap \
            python3-renderpm \
            python3-slugify \
            python3-num2words \
            python3-pdfminer \
            python3-phonenumbers \
            python3-qrcode \
            python3-setuptools \
            python3-watchdog \
            python3-wheel \
            python3-vobject \
            python3-xlrd \
            python3-xlwt \
            xfonts-base xfonts-75dpi \
            xz-utils \
            && curl -o wkhtmltox.deb -sSL https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.buster_amd64.deb \
            && echo 'ea8277df4297afc507c61122f3c349af142f31e5 wkhtmltox.deb' | sha1sum -c - \
            && dpkg --force-depends -i wkhtmltox.deb \
            && apt-get -y install -f --no-install-recommends \
            && rm -rf /var/lib/apt/lists/* wkhtmltox.deb

# install latest postgresql-client
RUN echo 'deb http://apt.postgresql.org/pub/repos/apt/ bullseye-pgdg main' > /etc/apt/sources.list.d/pgdg.list \
    && GNUPGHOME="$(mktemp -d)" \
    && export GNUPGHOME \
    && repokey='B97B0AFCAA1A47F044F244A07FCC7D46ACCC4CF8' \
    && gpg --batch --keyserver keyserver.ubuntu.com --recv-keys "${repokey}" \
    && gpg --batch --armor --export "${repokey}" > /etc/apt/trusted.gpg.d/pgdg.gpg.asc \
    && gpgconf --kill all \
    && rm -rf "$GNUPGHOME" \
    && apt-get update \
    && apt-get install --no-install-recommends -y postgresql-client \
    && rm -f /etc/apt/sources.list.d/pgdg.list \
    && rm -rf /var/lib/apt/lists/*

# Install Node.js v.x
ENV nodejs-version 17

RUN curl -o nodejs -sSL https://deb.nodesource.com/setup_${nodejs-version}.x | bash - \
    && apt-get update \
    && apt-get install -yqq --no-install-recommends nodejs

# Install rtlcss (on Debian bullseye)
RUN npm install -g rtlcss less@3.10.3 less-plugin-clean-css

# Copy entrypoint script and Odoo configuration file
COPY ./entrypoint.sh /
RUN adduser --system --quiet --shell=/bin/bash --no-create-home --gecos 'odoo' --group odoo

# location structure for odoo production
ENV ODOO_SRC odoo
RUN mkdir -p /opt/odoo/extra-addons \
    && mkdir -p /opt/odoo/data \
    && mkdir -p /opt/odoo/config \
    && mkdir -p /opt/odoo/enterprise \
    && mkdir -p /opt/odoo/COSTARICA \
    && mkdir -p /opt/odoo/FE \
    && mkdir -p /opt/odoo/log \
    && chown -R odoo /opt

COPY ./odoo.conf /opt/odoo/config/odoo.conf
RUN chown odoo /opt/odoo/config/odoo.conf

VOLUME ["/opt/odoo/data", "/opt/odoo/log","/opt/odoo/extra-addons", "/var/lib/odoo"]

#Install Odoo
ENV ODOO_VERSION 15.0
ENV DEPTH 1
ARG PATHBASE=/opt/odoo
ARG PATHREPOS=${PATHBASE}/enterprise
ARG COSTARICA=${PATHBASE}/COSTARICA
ARG FE=${PATHBASE}/FE

RUN git clone https://github.com/odoo/odoo.git -b ${ODOO_VERSION} --depth ${DEPTH} ${PATHBASE}/odoo


#Install modules Location FE
#RUN git clone https://github.com/josuecocoman/FE-CR/tree/odoo_15 -b ${ODOO_VERSION} --depth ${DEPTH} ${FE}


#Install modules personalized COSTARICA
#RUN git clone https://github.com/josuecocoman/Addons/tree/odoo_15 -b ${ODOO_VERSION} --depth ${DEPTH} ${COSTARICA}


#Install modules personalized COSTARICA
RUN git clone https://gitlab.com/josuecocoman/enterprise.git -b ${ODOO_VERSION} --depth ${DEPTH} ${PATHREPOS}

#Instalar Requerimientos Odoo
RUN sed -i "s/psycopg2==2.7.3.1; sys_platform != 'win32'/psycopg2==2.7.3.1; sys_platform != 'win32' and python_version < '3.8'/g" ${PATHBASE}/odoo/requirements.txt
RUN sed -i "s/psycopg2==2.8.3; sys_platform == 'win32'/psycopg2==2.8.3; sys_platform == 'win32' or python_version >= '3.8'/g" ${PATHBASE}/odoo/requirements.txt
RUN sed -i "s/lxml==3.7.1 ; sys_platform != 'win32'/lxml; sys_platform != 'win32' and python_version < '3.7'/g" ${PATHBASE}/odoo/requirements.txt
RUN sed -i '/libsass/d' ${PATHBASE}/odoo/requirements.txt
RUN pip3 install --upgrade pip
RUN apt update && apt-get upgrade -y\
    && pip3 install -r /opt/odoo/odoo/requirements.txt

# library for module installation and system
RUN pip3 install pypdf2 gevent\
    microsoftgraph-python \
    unicodecsv \
    websocket-client \
    pysftp==0.2.8 \
    pem \
    tree \
    nano \
    pygresql \
    pyopenssl \
    xmltodict \
    dicttoxml \
    signxml \
    xmlsig \
    crypto \
    cryptography \
    cchardet \
    pypng \
    psycopg2-binary \
    libsass \
    requests \
    pathlib \
    pytz==2019.1 \
    python-dateutil \
    pdf2image \
    PyMuPDF \
    PyQRCode \
    xades \
    pandas \
    boto \
    rotate_backups_s3 \
    oauthlib \
    simplejson \
    ofxparse \
    dbfread \
    ebaysdk \
    firebase_admin \
    jsonschema \
    deep_translator \
    google_auth \
    six

RUN apt --fix-broken install
RUN apt-get -f -y install
RUN apt-get autoremove -y

# Expose Odoo services
EXPOSE 8069 8071 8072

# Set the default config file
ENV ODOO_RC /opt/odoo/config/odoo.conf

COPY wait-for-psql.py /usr/local/bin/wait-for-psql.py

# Set default user when running the container
RUN ln -s /opt/odoo/odoo/odoo-bin /usr/bin/odoo
RUN chown -R odoo: /var/lib/odoo
RUN chown -R odoo: /usr/local/bin/
RUN chown -R odoo: /opt

# Set default user when running the container
USER odoo

# CMD /usr/bin/odoo
ENTRYPOINT ["/bin/bash", "/entrypoint.sh"]
CMD ["odoo"]